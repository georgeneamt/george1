package george1;

public class Om {
	
	private String nume;
	private String prenume;
	private int varsta;
	private float greutate;
	
	public Om () {
		this.nume = null;
		this.prenume = null;
		this.varsta = 0;
		this.greutate = 0;
	}
	
	public Om (String n, String pn, int v, float g) {
		this.nume = n;
		this.prenume = pn;
		this.varsta = v;
		this.greutate = g;
	}
	
	public String getNume() {
		return nume;
	}
	public void setNume(String nume) {
		this.nume = nume;
	}
	public String getPrenume() {
		return prenume;
	}
	public void setPrenume(String prenume) {
		this.prenume = prenume;
	}
	public int getVarsta() {
		return varsta;
	}
	public void setVarsta(int varsta) {
		this.varsta = varsta;
	}
	public float getGreutate() {
		return greutate;
	}
	public void setGreutate(float greutate) {
		this.greutate = greutate;
	}
	
	public String toString(){
		return this.nume + " " + this.prenume + " " + this.varsta + " " + this.greutate;
	}
	
	
}
